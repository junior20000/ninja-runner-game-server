extends Node

signal auth_done()

var network = NetworkedMultiplayerENet.new()
var ip = "crawfish.ddns.net"
var port = 6243

func _ready():
	start_connection()
	
func start_connection():
	network.create_client(ip, port)
	get_tree().set_network_peer(network)
	
	network.connect("connection_failed", self, "on_connection_failed")
	network.connect("connection_succeeded", self, "on_connection_succeeded")

func authenticate(sid, username, password):
	print("Forwarding " + str(sid) + " authentication request...")
	rpc_id(1, "authenticate", sid, username, password)

remote func authentication_result(sid, response_code, response_msg, username):
	print("Auth result " + str(sid) + ": " + str(response_code) + " -> " + response_msg)
	Gateway.return_login_request(sid, response_code, response_msg, username)

func on_connection_succeeded():
	print("Auth Server Connected")

func on_connection_failed():
	print("Failed to connect to Auth Server")
