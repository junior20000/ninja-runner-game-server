extends HTTPRequest

var auth_url = "http://crawfish.ddns.net:2424/api/insert_score.php"

var network = NetworkedMultiplayerENet.new()
var gtwy_mpapi = MultiplayerAPI.new()
var port = 6969
var max_conn = 100

func _ready():
	start()
	
func _process(delta):
	if not custom_multiplayer.has_network_peer():
		return
	
	custom_multiplayer.poll()
	
func start():
	network.create_server(port, max_conn)
	set_custom_multiplayer(gtwy_mpapi)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	#get_tree().set_network_peer(network)
	print("Gateway started")
	
	network.connect("peer_connected", self, "on_peer_connected")
	network.connect("peer_disconnected", self, "on_peer_disconnected")

func return_login_request(sid, response_code, response_message, username):
	rpc_id(sid, "return_login_request", response_code, response_message, username)
	#network.disconnect_peer(sid)

# REMOTES #

remote func login_request(username, password):
	var sid = custom_multiplayer.get_rpc_sender_id()
	
	print("Connection " + str(sid) + ": login request")
	AuthServer.authenticate(sid, username, password)

remote func send_score(username, score, death_by):
	var sid = custom_multiplayer.get_rpc_sender_id()
	print("Connection " + str(sid) + ": send score request")
	
	var body = {"username": username,
	"score": score,
	"death_by": death_by}
	
	request(auth_url, ["Content-Type: application/json"], false, HTTPClient.METHOD_POST, to_json(body))

# REMOTES END #

func on_score_sent(result, response_code, headers, body):
	print("Score sent!")

func on_peer_connected(conn_id):
	print("User " + str(conn_id) + " connected")
	
func on_peer_disconnected(conn_id):
	print("User " + str(conn_id) + " disconnected")
